-- LUALOCALS < ---------------------------------------------------------
local ItemStack, ipairs, math, minetest, nodecore, pairs, string, table
    = ItemStack, ipairs, math, minetest, nodecore, pairs, string, table
local math_floor, math_random, string_sub, table_concat
    = math.floor, math.random, string.sub, table.concat
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function curse_get(stack)
	local s = stack:get_meta():get_string(modname)
	return s and minetest.deserialize(s) or nil
end
local function curse_set(stack, data)
	stack:get_meta():set_string(modname,
		data and minetest.serialize(data) or "")
end

local invcache = {}
local function cursinv_get(player)
	local pname = player:get_player_name()
	local cached = invcache[pname]
	if cached then return cached end
	local inv = minetest.create_detached_inventory(
		modname .. "_" .. pname, {}, pname)
	inv:set_size(modname, player:get_inventory():get_size("main"))
	local meta = player:get_meta()
	local s = meta:get_string(modname) or ""
	local items = s and minetest.deserialize(s) or {}
	for i, v in ipairs(items) do
		inv:set_stack(modname, i, ItemStack(v or ""))
	end
	cached = {
		inv = inv,
		save = function()
			local t = {}
			for i = 1, inv:get_size(modname) do
				t[i] = inv:get_stack(modname, i):to_string()
			end
			meta:set_string(modname, minetest.serialize(t))
		end
	}
	invcache[pname] = cached
	return cached
end

local curseitem = modname .. ":cursed"
nodecore.register_virtual_item(curseitem, {
		description = "",
		inventory_image = "[combine:1x1",
		hotbar_type = "cursed",
	})

nodecore.register_healthfx({
		item = curseitem,
		getqty = function(player)
			local cursinv = cursinv_get(player)
			local total = cursinv.inv:get_size(modname)
			local cursed = 1 -- consume the "freebie" slot
			for i = 1, total do
				if not cursinv.inv:get_stack(modname, i):is_empty() then
					cursed = cursed + 1
				end
			end
			return cursed / total
		end
	})

nodecore.register_playerstep({
		label = "apply curses",
		action = function(player)
			local cursed = {}
			local keys = {}
			local slots = {}

			local inv = player:get_inventory()
			for i = 1, inv:get_size("main") do
				local stack = inv:get_stack("main", i)
				if stack:get_name() == curseitem then
					slots[#slots + 1] = i
				else
					local data = curse_get(stack)
					if data and data.id then
						cursed[data.id] = {
							stack = stack,
							slot = i
						}
					elseif data and data.key then
						keys[data.key] = true
					end
				end
			end

			local cursinv = cursinv_get(player)
			local dirty
			for k, v in pairs(cursed) do
				if not keys[k] then
					dirty = true
					local exist = cursinv.inv:get_stack(modname, v.slot)
					v.stack = exist:add_item(v.stack)
					cursinv.inv:set_stack(modname, v.slot, exist)
					if not v.stack:is_empty() then
						v.stack = cursinv.inv:add_item(modname, v.stack)
					end
					inv:set_stack("main", v.slot, v.stack)
				end
			end
			if dirty then
				nodecore.setphealth(player, 0)
				cursinv.save()
				dirty = nil
			end

			for i = 1, cursinv.inv:get_size(modname) do
				local stack = cursinv.inv:get_stack(modname, i)
				if not stack:is_empty() then
					local curse = curse_get(stack)
					if (not curse) or keys[curse.id] then
						dirty = true
						cursinv.inv:set_stack(modname, i, ItemStack(""))
						if #slots > 0 then
							inv:set_stack("main", slots[#slots], stack)
							slots[#slots] = nil
						else
							stack = nodecore.give_item(player, stack, "main")
							if not stack:is_empty() then
								local pos = player:get_pos()
								pos.y = pos.y + player:get_properties().eye_height
								nodecore.item_eject(pos, stack)
							end
						end
					end
				end
			end
			if dirty then cursinv.save() end
		end
	})

local keyitem = modname .. ":key"
minetest.register_craftitem(keyitem, {
		description = "Anodyne",
		inventory_image = modname .. "_key.png",
		on_stack_touchtip = function(stack, desc)
			local data = curse_get(stack)
			if data and data.owner then desc = desc .. "\n" .. data.owner end
			if data and data.desc then desc = desc .. "\n" .. data.desc end
			return desc
		end,
		on_use = function(tool, player)
			if not (player and player.get_inventory) then return end
			local inv = player:get_inventory()
			if not inv then return end
			local curse = curse_get(tool)
			if not (curse and curse.key) then return end
			for i = 1, inv:get_size("main") do
				local stack = inv:get_stack("main", i)
				local data = curse_get(stack)
				if data and data.id == curse.key then
					curse_set(stack)
					inv:set_stack("main", i, stack)
					return ItemStack("")
				end
			end
		end
	})

local genkey
do
	local alpha = "BCDFGHJKPQSTVWXZ"
	genkey = function()
		local n = {}
		for i = 1, 32 do
			local r = math_floor(math_random() * #alpha + 1)
			n[i] = string_sub(alpha, r, r)
		end
		return table_concat(n)
	end
end

minetest.register_chatcommand("curse", {
		description = "curse currently wielded item",
		privs = {give = true},
		func = function(pname)
			local player = minetest.get_player_by_name(pname)
			if not player then return false, "player must be logged in" end

			local stack = player:get_wielded_item()
			if stack:is_empty() or nodecore.item_is_virtual(stack) then
				return false, "wield item empty"
			end
			local def = stack:get_definition()
			if (not def) or (def.type == "node") then
				return false, "item cannot be cursed"
			end

			local curse = curse_get(stack)
			if curse then return false, "item already cursed" end

			curse = {
				id = genkey()
			}
			curse_set(stack, curse)
			player:set_wielded_item(stack)

			local tool = ItemStack(keyitem)
			local keydata = {
				key = curse.id,
				owner = pname,
				desc = nodecore.touchtip_stack(stack, true)
			}
			curse_set(tool, keydata)
			tool = nodecore.give_item(player, tool, "main")
			if not tool:is_empty() then
				local pos = player:get_pos()
				pos.y = pos.y + player:get_properties().eye_height
				nodecore.item_eject(pos, tool, 0.001)
			end

			return true, "cursed " .. keydata.desc
		end
	})
